<?php
/**
 * cada uno es una ruta que accede a la api
 * 
 */
return  [
    [
        'class' => 'yii\rest\UrlRule',
        'pluralize' => false,
        'controller' => ['servicios'],
    ], [
        'class' => 'yii\rest\UrlRule',
        'pluralize' => false,
        'controller' => ['noticias'],
    ],
    [
        'class' => 'yii\rest\UrlRule',
        'pluralize' => false,
        'controller' => ['productos-usuario'],//problemas con camelkeys (mayusculas)
        'extraPatterns' => [
            'POST cantidad' => 'cantidad',
            'OPTIONS cantidad' => 'cantidad',
        ]
    ],
    [
        'class' => 'yii\rest\UrlRule',
        'pluralize' => false,
        'controller' => ['linea-factura'],
        'extraPatterns' => [
            'POST factura' => 'factura',
            'OPTIONS factura' => 'factura',
        ]
    ],
    [
        'class' => 'yii\rest\UrlRule',
        'pluralize' => false,
        'controller' => ['factura'],
        'extraPatterns' => [
            'POST facturas' => 'facturas',
            'OPTIONS facturas' => 'facturas',
        ]
    ],
    
    [
        'class' => 'yii\rest\UrlRule',
        'pluralize' => false,
        'controller' => ['usuarios'],
    ],
    [
        'class' => 'yii\rest\UrlRule',
        'pluralize' => false,
        'controller' => ['beneficios'],
    ],
    [
        'class' => 'yii\rest\UrlRule',
        'pluralize' => false,
        'controller' => ['gastos'],
    ],
    [
        'class' => 'yii\rest\UrlRule',
        'pluralize' => false,
        'controller' => ['servicioscampo'],
    ],
    [
        'class' => 'yii\rest\UrlRule',//login
        'controller' => ['user'],
        'pluralize' => false,
        'extraPatterns' => [
            'POST authenticate' => 'authenticate',
            'OPTIONS authenticate' => 'authenticate',
            'POST register' => 'register',
            'OPTIONS register' => 'register',
        ]
    ],
    [
        'class' => 'yii\rest\UrlRule',
        'pluralize' => false,
        'extraPatterns' => [
            'GET coop' => 'coop',
            'OPTIONS coop' => 'coop',
            'POST coop' => 'coop',
        ]
    ]
];
