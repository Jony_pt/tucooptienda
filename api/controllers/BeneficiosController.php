<?php

namespace app\controllers;

use yii\rest\ActiveController;
use app\models\ProductosUsuario;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBearerAuth;

class BeneficiosController extends  ApiController
{
    public $modelClass = 'app\models\ProductoUsuario';

    public $authenable = false;

    public function actions()
    {
        $actions = parent::actions();
        //Eliminamos acciones de crear y eliminar apuntes. Eliminamos update para personalizarla
        unset($actions['delete'], $actions['create'], $actions['update']);
        // Redefinimos el método que prepara los datos en el index
        $actions['index']['prepareDataProvider'] = [$this, 'indexProvider'];
        return $actions;
    }

    public function indexProvider()
    {
        $usu = $_GET["usuario"] ?? "";//esto no se debe de hacer asi, yii::user->identity->id
        if ($usu == "") {
            return new ActiveDataProvider([//para que la api devuleva lso datos necesita un objeto activedataprovider
                'query' => ProductosUsuario::find()
            ]);
        } else {
            return new ActiveDataProvider([
                'query' => ProductosUsuario::find()->select(["temporada_id", "beneficio", "fecha", "producto_id", "usuario_id"])->where("usuario_id = " . $usu)
            ]);
        }
    }
}
