<?php

namespace app\controllers;
use app\models\ServicioCampo;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBearerAuth;

class GastosController extends  ApiController
{
    public $modelClass = 'app\models\ServicioCampo';

    public $authenable=false;
    
    public function actions()
    {
        $actions = parent::actions();
        //Eliminamos acciones de crear y eliminar apuntes. Eliminamos update para personalizarla
        unset($actions['delete'], $actions['create'], $actions['update']);
        // Redefinimos el método que prepara los datos en el index
        $actions['index']['prepareDataProvider'] = [$this, 'indexProvider'];
        return $actions;
    }

    public function indexProvider()
    {
        $usu = $_GET["usuario"] ?? "";
        if($usu == ""){
            return new ActiveDataProvider([
                'query' => ServicioCampo::find()
            ]);
        }else{
            return new ActiveDataProvider([
                'query' => ServicioCampo::find()->select(["temporada_id", "beneficio", "gasto", "fecha_contrato", "servicio_id", "trabajador_id", "campo_id"])->where("`campo_id` IN (SELECT campo_id FROM campo where usuario_id = $usu )")
            ]);
        }
    }
}