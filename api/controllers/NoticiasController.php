<?php

namespace app\controllers;

use yii\filters\Cors;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use app\models\Noticias;

class NoticiasController extends  ApiController
{
    public $modelClass = 'app\models\Noticias';

    public $authenable = false;

    public function actions()
    {
        $actions = parent::actions();
        //Eliminamos acciones de crear y eliminar apuntes. Eliminamos update para personalizarla
        unset($actions['delete'], $actions['create'], $actions['update']);
        // Redefinimos el método que prepara los datos en el index
        $actions['index']['prepareDataProvider'] = [$this, 'indexProvider'];
        return $actions;
    }
    
    function indexProvider()
    {
        $cooperativa = $_GET['cooperativa']??"";

        if ($cooperativa!="") {
            return new ActiveDataProvider([
                'query' => Noticias::find()
                    ->where(['cooperativa_id' => $cooperativa])
            ]);
        } else {
            return new ActiveDataProvider([
                'query' => Noticias::find()

            ]);
        }
    }
}
