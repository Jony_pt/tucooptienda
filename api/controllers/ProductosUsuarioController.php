<?php

namespace app\controllers;

use app\models\Producto;
use yii\rest\ActiveController;
use app\models\ProductosUsuario;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBearerAuth;

class ProductosUsuarioController extends  ApiController 
{
    public $modelClass = 'app\models\ProductosUsuario';
    
    public $authenable=false;
    
    public function actions()
    {
        $actions = parent::actions();
        //Eliminamos acciones de crear y eliminar apuntes. Eliminamos update para personalizarla
        unset($actions['delete'], $actions['create'], $actions['update']);
        // Redefinimos el método que prepara los datos en el index
        $actions['index']['prepareDataProvider'] = [$this, 'indexProvider'];
        return $actions;
    }

    public function indexProvider()
    {
        $usu = $_GET["usuario"] ?? "";
        if($usu == ""){
            return new ActiveDataProvider([
                'query' => ProductosUsuario::find()->select(["temporada_id", "producto_id", "sum(cantidad) as kilos"])->groupBy(["producto_id", "temporada_id"])
            ]);
        }else{
            return new ActiveDataProvider([
                'query' => ProductosUsuario::find()->select(["temporada_id", "producto_id", "sum(cantidad) as kilos", "fecha"])->where('usuario_id=' . $usu)->groupBy(["producto_id", "temporada_id"])
            ]);
        }
        
    }
}
