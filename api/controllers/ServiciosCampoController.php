<?php

namespace app\controllers;
use yii\filters\Cors;
use app\models\ServicioCampo;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBearerAuth;

class ServiciosCampoController extends  ApiController
{
    public $modelClass = 'app\models\ServicioCampo';
    

    public function actions()
    {
        $actions = parent::actions();
        //Eliminamos acciones de crear y eliminar apuntes. Eliminamos update para personalizarla
        unset($actions['delete'], $actions['create'], $actions['update']);
        // Redefinimos el método que prepara los datos en el index
        $actions['index']['prepareDataProvider'] = [$this, 'indexProvider'];
        return $actions;
    }

    public function indexProvider()
    {
        $usuario = $_GET["usuario"] ?? "";
        if($usuario == ""){
            return new ActiveDataProvider([
                'query' => ServicioCampo::find()
            ]);
        }else{
            return new ActiveDataProvider([
                'query' => ServicioCampo::find()
                ->where("campo_id IN (SELECT id FROM campo where usuario_id = $usuario)")
                ->orderBy("fecha_contrato")
            ]);
        }
    }

    public $authenable=false;
}
