<?php

namespace app\controllers;
use yii\rest\ActiveController;
 
class ServiciosController extends  ApiController
{
    public $modelClass = 'app\models\Servicios';//de que modelo tira el controlador
    
    public $authenable=false;//en este caso, aqui no interesa el token
}