<?php
//aqui se gestiona lo del login con el front
namespace app\controllers;

use yii\rest\ActiveController;

class UserController extends  ApiController
{
    public $modelClass = 'app\models\Usuarios';

    public $authenable=false;

    public function actionAuthenticate()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // Si se envían los datos en formato raw dentro de la petición http, se recogen así:
            $params = json_decode(file_get_contents("php://input"), false);
            @$username = $params->username;//los datos de ingreso del login (se cogen por parametro)
            @$password = $params->password;
            // Si se envían los datos de la forma habitual (form-data), se reciben en $_POST:

            //$username=$_POST['username'];
            //$password=$_POST['password'];

            if ($u = \app\models\Usuarios::findOne(['mail' => $username])) {//si se encuentra el correo pasado como parametro en la tabla usuarios, lo guardo en $u
                if (isset($password)) {//si existe la pass (si has escrito una pass para el login)
                    if ($u->contrasenya == md5($password)) { //pasamos la pass del parametro a md5 y vemos si coincide con la pass de la bd
                        $bytes = openssl_random_pseudo_bytes(32);//genero bits aleatorios de forma segura
                        $hex = bin2hex($bytes);//los convierto en hexadecimal
                        $u->token = $hex;//se queda como el token del ususario
                        $u->save();//guardo en la bd
                        return ['token' => $u->token, 'id' => $u->id, 'nombre' => $u->nombre,  'apellidos' => $u->apellidos, 'mail' => $u->mail, 'direccion' => $u->direccion, 'cp' => $u->codigo_postal, 'imagen_src' => $u->imagen_src, 'dni' => $u->dni,];
                    }   //devuelve todos los datos cuando loguea
                }                
            }
            return ['error' => 'Usuario incorrecto. ' . $username];//si no existe el usuario/pass, mensaje de error
        }
    }
}
