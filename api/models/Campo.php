<?php
//los modelos son las clases de java, donde estan todos los metodos a usar. como un mvc pero sin vistas
namespace app\models;

use Yii;

/**
 * This is the model class for table "campo".
 *
 * @property int $id
 * @property int $usuario_id
 * @property int $tamanyo
 * @property string $direccion
 * @property string $ciudad
 * @property int $codigo_postal
 *
 * @property Usuario $usuario
 * @property ServicioCampo[] $servicioCampos
 */
class Campo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'campo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usuario_id', 'tamanyo', 'direccion', 'ciudad', 'codigo_postal'], 'required'],
            [['usuario_id', 'tamanyo', 'codigo_postal'], 'integer'],
            [['direccion', 'ciudad'], 'string', 'max' => 30],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['usuario_id' => 'id']],
            [['cooperativa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cooperativa::className(), 'targetAttribute' => ['cooperativa_id' => 'id']],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_id' => 'Usuario ID',
            'tamanyo' => 'Tamanyo',
            'direccion' => 'Direccion',
            'ciudad' => 'Ciudad',
            'codigo_postal' => 'Codigo Postal',
            'cooperativa_id'=> 'Cooperativa ID',
        ];
    }

    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuarios::className(), ['id' => 'usuario_id']);
    }

    /**
     * Gets query for [[ServicioCampos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getServicioCampos()
    {
        return $this->hasMany(ServicioCampo::className(), ['campo_id' => 'id']);
    }
}
