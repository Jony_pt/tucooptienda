<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gasto".
 *
 * @property int $id
 * @property int $servicio_id
 * @property string $concepto
 * @property string $descripcion
 * @property string $fecha
 * @property int $trabajador_id
 * @property int $total
 *
 * @property ServicioCampo $servicio
 * @property Trabajador $trabajador
 */
class Gasto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gasto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['servicio_id', 'concepto', 'descripcion', 'fecha', 'trabajador_id', 'total'], 'required'],
            [['servicio_id', 'trabajador_id', 'total'], 'integer'],
            [['fecha'], 'safe'],
            [['concepto'], 'string', 'max' => 60],
            [['descripcion'], 'string', 'max' => 200],
            [['servicio_id'], 'exist', 'skipOnError' => true, 'targetClass' => ServicioCampo::className(), 'targetAttribute' => ['servicio_id' => 'id']],
            [['trabajador_id'], 'exist', 'skipOnError' => true, 'targetClass' => Trabajador::className(), 'targetAttribute' => ['trabajador_id' => 'id']],
            [['cooperativa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cooperativa::className(), 'targetAttribute' => ['cooperativa_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'servicio_id' => 'Servicio ID',
            'concepto' => 'Concepto',
            'descripcion' => 'Descripcion',
            'fecha' => 'Fecha',
            'trabajador_id' => 'Trabajador ID',
            'total' => 'Total',
        ];
    }

    /**
     * Gets query for [[Servicio]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getServicio()
    {
        return $this->hasOne(ServicioCampo::className(), ['id' => 'servicio_id']);
    }

    /**
     * Gets query for [[Trabajador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrabajador()
    {
        return $this->hasOne(Trabajador::className(), ['id' => 'trabajador_id']);
    }
}
