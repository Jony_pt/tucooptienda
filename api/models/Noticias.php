<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "noticia".
 *
 * @property int $id
 * @property string $fecha_hora
 * @property string $titular
 * @property string $cuerpo
 * @property string $imagen_src
 */
class Noticias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'noticia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_hora', 'titular', 'cuerpo', 'imagen_src'], 'required'],
            [['fecha_hora'], 'safe'],
            [['titular'], 'string', 'max' => 30],
            [['cuerpo'], 'string', 'max' => 1000],
            [['imagen_src'], 'string', 'max' => 60],
            [['cooperativa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cooperativa::className(), 'targetAttribute' => ['cooperativa_id' => 'id']],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha_hora' => 'Fecha Hora',
            'titular' => 'Titular',
            'cuerpo' => 'Cuerpo',
            'imagen_src' => 'Imagen Src',
            'cooperativa_id'=>'Cooperativa Id',
        ];
    }
}
