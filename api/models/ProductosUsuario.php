<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "producto_usuario".
 *
 * @property int $id
 * @property int $usuario_id
 * @property int $producto_id
 * @property int $temporada_id
 * @property int $cantidad
 * @property string $fecha
 * @property int|null $gasto
 * @property int|null $beneficio
 *
 * @property Producto $producto
 * @property Temporada $temporada
 * @property Usuario $usuario
 */
class ProductosUsuario extends \yii\db\ActiveRecord
{
    public $kilos;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'producto_usuario';
    }
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usuario_id', 'producto_id', 'temporada_id', 'cantidad', 'fecha'], 'required'],
            [['usuario_id', 'producto_id', 'temporada_id', 'cantidad', 'gasto', 'beneficio', 'kilos'], 'integer'],
            [['fecha'], 'safe'],
            [['producto_id'], 'exist', 'skipOnError' => true, 'targetClass' => Producto::className(), 'targetAttribute' => ['producto_id' => 'id']],
            [['temporada_id'], 'exist', 'skipOnError' => true, 'targetClass' => Temporada::className(), 'targetAttribute' => ['temporada_id' => 'id']],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['usuario_id' => 'id']],
            [['cooperativa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cooperativa::className(), 'targetAttribute' => ['cooperativa_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'usuario_id' => 'Usuario ID',
            'producto_id' => 'Producto ID',
            'temporada_id' => 'Temporada ID',
            'cantidad' => 'Cantidad',
            'fecha' => 'Fecha',
            'gasto' => 'Gasto',
            'beneficio' => 'Beneficio',
            'kilos'
        ];
    }

    /**
     * Gets query for [[Producto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProducto()
    {
        return $this->hasOne(Producto::className(), ['id' => 'producto_id']);
    }

    /**
     * Gets query for [[Temporada]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTemporada()
    {
        return $this->hasOne(Temporada::className(), ['id' => 'temporada_id']);
    }

    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuarios::className(), ['id' => 'usuario_id']);
    }

    public function getproducto_nombre()
    {
        return $this->producto->nombre;
    }

    public function fields()
    {
        $fields = parent::fields();
        unset($fields["producto_id"]);
        $fields[] = 'producto_nombre';
        $fields[] = 'kilos';
        return $fields;
    }
}
