<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "servicio_campo".
 *
 * @property int $id
 * @property int $servicio_id
 * @property int $campo_id
 * @property int $temporada_id
 * @property int $gasto
 * @property string $fecha_contrato
 * @property string $fecha_vencimiento
 * @property int $beneficio
 * @property int $trabajador_id
 *
 * @property Gasto[] $gastos
 * @property Trabajador $trabajador
 * @property Campo $campo
 * @property Servicio $servicio
 * @property Temporada $temporada
 */
class ServicioCampo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'servicio_campo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['servicio_id', 'campo_id', 'temporada_id', 'gasto', 'fecha_contrato', 'fecha_vencimiento', 'beneficio', 'trabajador_id'], 'required'],
            [['servicio_id', 'campo_id', 'temporada_id', 'gasto', 'beneficio', 'trabajador_id'], 'integer'],
            [['fecha_contrato', 'fecha_vencimiento'], 'safe'],
            [['trabajador_id'], 'exist', 'skipOnError' => true, 'targetClass' => Trabajador::className(), 'targetAttribute' => ['trabajador_id' => 'id']],
            [['campo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Campo::className(), 'targetAttribute' => ['campo_id' => 'id']],
            [['servicio_id'], 'exist', 'skipOnError' => true, 'targetClass' => Servicios::className(), 'targetAttribute' => ['servicio_id' => 'id']],
            [['temporada_id'], 'exist', 'skipOnError' => true, 'targetClass' => Temporada::className(), 'targetAttribute' => ['temporada_id' => 'id']],
            [['cooperativa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cooperativa::className(), 'targetAttribute' => ['cooperativa_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'servicio_id' => 'Servicio ID',
            'campo_id' => 'Campo ID',
            'temporada_id' => 'Temporada ID',
            'gasto' => 'Gasto',
            'fecha_contrato' => 'Fecha Contrato',
            'fecha_vencimiento' => 'Fecha Vencimiento',
            'beneficio' => 'Beneficio',
            'trabajador_id' => 'Trabajador ID',
        ];
    }

    /**
     * Gets query for [[Gastos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGastos()
    {
        return $this->hasMany(Gasto::className(), ['servicio_id' => 'id']);
    }

    /**
     * Gets query for [[Trabajador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrabajador()
    {
        return $this->hasOne(Trabajador::className(), ['id' => 'trabajador_id']);
    }

    /**
     * Gets query for [[Campo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCampo()
    {
        return $this->hasOne(Campo::className(), ['id' => 'campo_id']);
    }

    /**
     * Gets query for [[Servicio]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getServicio()
    {
        return $this->hasOne(Servicios::className(), ['id' => 'servicio_id']);
    }

    /**
     * Gets query for [[Temporada]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTemporada()
    {
        return $this->hasOne(Temporada::className(), ['id' => 'temporada_id']);
    }

    public function getservicio_nombre()
    {
        return $this->servicio->nombre;
    }

    public function gettrabajador_nombre()
    {
        return $this->trabajador->apellidos . ", " . $this->trabajador->nombre;
    }

    public function getcampo_nombre()
    {
        return $this->campo->ciudad . ", " . $this->campo->direccion;
    }

    public function getservicio_imagen()
    {
        return $this->servicio->imagen_src;
    }
    public function fields()//lo que muestra la api, por defecto devuelve todos los atributos*
    //*->si es por defecto, el metodo fields no estará escrito, si quieres modificar lo que muestra la api, tendras que escribirlo
    {
        $fields = parent::fields();
        unset($fields["servicio_id"]);
        unset($fields["trabajador_id"]);
        unset($fields["campo_id"]);
        $fields[] = 'servicio_nombre';
        $fields[] = 'trabajador_nombre';
        $fields[] = 'campo_nombre';
        $fields[] = 'servicio_imagen';
        return $fields;
    }
}
