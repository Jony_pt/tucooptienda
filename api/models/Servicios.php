<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "servicio".
 *
 * @property int $id
 * @property string $nombre
 * @property int $precio_metro
 *
 * @property ServicioCampo[] $servicioCampos
 */
class Servicios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'servicio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'precio_metro'], 'required'],
            [['precio_metro'], 'integer'],
            [['nombre'], 'string', 'max' => 60],
            [['cooperativa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cooperativa::className(), 'targetAttribute' => ['cooperativa_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'precio_metro' => 'Precio Metro',
        ];
    }

    /**
     * Gets query for [[ServicioCampos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getServicioCampos()
    {
        return $this->hasMany(ServicioCampo::className(), ['servicio_id' => 'id']);
    }
}
