<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trabajador".
 *
 * @property int $id
 * @property string $nombre
 * @property string $apellidos
 * @property string $dni
 * @property string $mail
 * @property string $telefono
 * @property string $direccion
 * @property string $ciudad
 * @property int $codigo_postal
 * @property string $IBAN
 *
 * @property Gasto[] $gastos
 * @property ServicioCampo[] $servicioCampos
 */
class Trabajador extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trabajador';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'apellidos', 'dni', 'mail', 'telefono', 'direccion', 'ciudad', 'codigo_postal', 'IBAN'], 'required'],
            [['codigo_postal'], 'integer'],
            [['nombre', 'mail', 'ciudad'], 'string', 'max' => 30],
            [['apellidos', 'direccion'], 'string', 'max' => 60],
            [['dni', 'telefono'], 'string', 'max' => 9],
            [['IBAN'], 'string', 'max' => 26],
            [['cooperativa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cooperativa::className(), 'targetAttribute' => ['cooperativa_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'dni' => 'Dni',
            'mail' => 'Mail',
            'telefono' => 'Telefono',
            'direccion' => 'Direccion',
            'ciudad' => 'Ciudad',
            'codigo_postal' => 'Codigo Postal',
            'IBAN' => 'Iban',
        ];
    }

    /**
     * Gets query for [[Gastos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGastos()
    {
        return $this->hasMany(Gasto::className(), ['trabajador_id' => 'id']);
    }

    /**
     * Gets query for [[ServicioCampos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getServicioCampos()
    {
        return $this->hasMany(ServicioCampo::className(), ['trabajador_id' => 'id']);
    }
}
