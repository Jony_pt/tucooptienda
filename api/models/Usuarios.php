<?php

namespace app\models;

use Yii;
use app\models\Campo;
use yii\web\IdentityInterface;
use app\models\ProductosUsuario;

/**
 * This is the model class for table "usuario".
 *
 * @property int $id
 * @property string $nombre
 * @property string $apellidos
 * @property string $mail
 * @property string $direccion
 * @property string $ciudad
 * @property int $codigo_postal
 * @property int $telefono
 * @property string $dni
 * @property string $rol
 * @property string $contrasenya
 * @property int $cuota
 * @property string $IBAN
 * @property string $imagen_src
 *
 * @property Campo[] $campos
 * @property ProductoUsuario[] $productoUsuarios
 */
class Usuarios extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'apellidos', 'mail', 'direccion', 'ciudad', 'codigo_postal', 'telefono', 'dni', 'rol', 'contrasenya', 'cuota', 'IBAN', 'imagen_src'], 'required'],
            [['codigo_postal', 'telefono', 'cuota'], 'integer'],
            [['nombre', 'apellidos', 'mail', 'ciudad'], 'string', 'max' => 30],
            [['direccion', 'imagen_src'], 'string', 'max' => 60],
            [['dni'], 'string', 'max' => 9],
            [['rol'], 'string', 'max' => 1],
            [['contrasenya'], 'string', 'max' => 32],
            [['IBAN'], 'string', 'max' => 26],
            [['token'], 'string', 'max' => 128],
            [['cooperativa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cooperativa::className(), 'targetAttribute' => ['cooperativa_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'mail' => 'Mail',
            'direccion' => 'Direccion',
            'ciudad' => 'Ciudad',
            'codigo_postal' => 'Codigo Postal',
            'telefono' => 'Telefono',
            'dni' => 'Dni',
            'rol' => 'Rol',
            'contrasenya' => 'Contrasenya',
            'cuota' => 'Cuota',
            'IBAN' => 'Iban',
            'imagen_src' => 'Imagen Src',
            'token' => 'token',
        ];
    }

    /**
     * Gets query for [[Campos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCampos()
    {
        return $this->hasMany(Campo::className(), ['usuario_id' => 'id']);
    }

    /**
     * Gets query for [[ProductoUsuarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductoUsuarios()
    {
        return $this->hasMany(ProductosUsuario::className(), ['usuario_id' => 'id']);
    }

    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['token' => $token]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->authKey;
    }

    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }
}
