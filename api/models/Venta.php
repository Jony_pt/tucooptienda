<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "venta".
 *
 * @property int $id
 * @property int $producto_id
 * @property string $fecha
 * @property int $precio
 * @property string $comprador
 * @property int $cantidad
 * @property int $temporada_id
 *
 * @property Producto $producto
 * @property Temporada $temporada
 */
class Venta extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'venta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['producto_id', 'fecha', 'precio', 'comprador', 'cantidad', 'temporada_id'], 'required'],
            [['producto_id', 'precio', 'cantidad', 'temporada_id'], 'integer'],
            [['fecha'], 'safe'],
            [['comprador'], 'string', 'max' => 60],
            [['producto_id'], 'exist', 'skipOnError' => true, 'targetClass' => Producto::className(), 'targetAttribute' => ['producto_id' => 'id']],
            [['temporada_id'], 'exist', 'skipOnError' => true, 'targetClass' => Temporada::className(), 'targetAttribute' => ['temporada_id' => 'id']],
            [['cooperativa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cooperativa::className(), 'targetAttribute' => ['cooperativa_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'producto_id' => 'Producto ID',
            'fecha' => 'Fecha',
            'precio' => 'Precio',
            'comprador' => 'Comprador',
            'cantidad' => 'Cantidad',
            'temporada_id' => 'Temporada ID',
        ];
    }

    /**
     * Gets query for [[Producto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProducto()
    {
        return $this->hasOne(Producto::className(), ['id' => 'producto_id']);
    }

    /**
     * Gets query for [[Temporada]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTemporada()
    {
        return $this->hasOne(Temporada::className(), ['id' => 'temporada_id']);
    }
}
