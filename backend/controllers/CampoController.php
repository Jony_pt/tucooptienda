<?php

namespace app\controllers;

use Yii;
use app\models\Campo;
use yii\web\Controller;
use app\models\CampoSearch;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;

/**
 * CampoController implements the CRUD actions for Campo model.
 */
class CampoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [//limitar los accesos
                'class' => AccessControl::className(),
                'only' => ['create', 'delete', 'update'],
                'rules' => [
                    [   //solo el que tenga el rol de admin puede hacer las acciones, esto esta en todos los controller
                        'allow' => true,
                        'actions' => ['create', 'delete', 'update'],
                        'matchCallback' => function ($rule, $action) {
                            return !Yii::$app->user->isGuest && Yii::$app->user->identity->rol == "A";
                        }
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Campo models.
     * @return mixed
     */
    public function actionIndex() //index.html
    {
        $searchModel = new CampoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [//se pasan los datos del modelo, render carga la pagina especificada (en este caso index)
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Campo model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)//la vista que muestra al seleccionar un campo (cuando le das click al ojo para ver el detalle)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Campo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()//lo mismo pero pa crear
    {
        $model = new Campo();
        /**
         * el metodo load lee los datos que ha escrito el usuario en el formulario a traves del post
         * despues de esto lo guarda
         */
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);//si entra al if, es que se ha guardado los datos, y te redirige a la vista del campo creado
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Campo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)//lo mismo pero pa modificar
    {
        $model = $this->findModel($id);//los datos de antes de modificar

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [//si no recibe datos del post, primero se ejecuta esto
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Campo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)//lo mismo pero pa borrar
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Campo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Campo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Campo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionLookup($term) {//funcion para el autocompletar
        $results = [];
        foreach (Campo::find()->andwhere("(concat(ciudad , ' ', direccion) like :q )", [':q' => '%' . $term . '%'])->asArray()->all() as $model) {
            $nom = $model["ciudad"] . ", " . $model["direccion"];
            $results[] = [
                'id' => $model['id'],
                'label' => $nom,
             ];
            }
        return \yii\helpers\Json::encode($results);
    }
}
