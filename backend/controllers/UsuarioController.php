<?php

namespace app\controllers;

use app\models\Usuario;
use app\models\UsuarioSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * UsuarioController implements the CRUD actions for Usuario model.
 */
class UsuarioController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['create', 'delete', 'update'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create', 'delete', 'update'],
                        'matchCallback' => function ($rule, $action) {
                            return !Yii::$app->user->isGuest && Yii::$app->user->identity->rol == "A";
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Usuario models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UsuarioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Usuario model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Usuario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Usuario(['scenario'=>'create']);

        if ($model->load(Yii::$app->request->post())) {

            $model->imagen_src = UploadedFile::getInstance($model, 'imagen_src');
            $cod = uniqid();
            $model->imagen_src->saveAs('../web/imagenes/perfil/' . $cod . '.' . $model->imagen_src->extension);
            $model->imagen_src = $cod . '.' . $model->imagen_src->extension;
            $model->contrasenya = md5($model->contrasenya);

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Servicio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        //GUARDO LA IMG VIEJA
        $img = $model->imagen_src;
        $pas = $model->contrasenya;
        $model->contrasenya = "1";
        if ($model->load(Yii::$app->request->post())) {

            //SI NO TIENE IMAGEN, LA METO
            if ( $model->imagen_src == '') {

                $model->imagen_src = $img;
                
            } else {
                //BORRO LA IMG VIEJA
                unlink('.../web/imagenes/perfil/' . $img);

                $model->imagen_src = UploadedFile::getInstance($model, 'imagen_src');
                $cod = uniqid();
                $model->imagen_src->saveAs('../web/imagenes/perfil/' . $cod . '.' . $model->imagen_src->extension);
              
                $model->imagen_src = $cod . '.' . $model->imagen_src->extension;
            }
            
            if ($model->contrasenya == "1") { //si hay un 1 en el campo contraseña, se guarda la contraseña vieja
                $model->contrasenya = $pas;
            } else {//si no, se cambia a la nueva contraeña
                $model->contrasenya = md5($model->contrasenya);
            }

            if ($model->save()) {//guardamos datos y redirige
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Usuario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        //GUARDO LA FOTO EN UNA VARIABLE
        //$this->findModel($id)->delete();
        $borrar = $this->findModel($id);
        $img = $borrar->imagen_src;
        //SI NO HAY FOTO, BORRO SOLO DATOS
        if ($img == '') {
            $borrar->delete();
            //SINO, BORRO DATOS Y FOTO    
        } else {
            unlink(('../web/imagenes/perfil/' . $img));
            $borrar->delete();
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Usuario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Usuario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Usuario::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionLookup($term)
    {
        $results = [];
        foreach (Usuario::find()->andwhere("(concat(apellidos , ' ', nombre) like :q )", [':q' => '%' . $term . '%'])->asArray()->all() as $model) {
            $nom = $model["apellidos"] . ", " . $model["nombre"];
            $results[] = [
                'id' => $model['id'],
                'label' => $nom,
            ];
            return \yii\helpers\Json::encode($results);
        }
    }
}
