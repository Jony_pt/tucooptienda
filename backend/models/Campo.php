<?php

namespace app\models;

use Yii;
use Yii\Helpers\Url;

/**
 * This is the model class for table "campo".
 *
 * @property int $id
 * @property int $usuario_id
 * @property int $tamanyo
 * @property string $direccion
 * @property string $ciudad
 * @property int $codigo_postal
 *
 * @property Usuario $usuario
 * @property ServicioCampo[] $servicioCampos
 */
class Campo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'campo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usuario_id', 'tamanyo', 'direccion', 'ciudad', 'codigo_postal'], 'required'],
            [['usuario_id', 'tamanyo', 'codigo_postal'], 'integer'],
            [['direccion', 'ciudad'], 'string', 'max' => 30],
            [['usuario_id'], 'exist', 'skipOnError' => true, 'targetClass' => Usuario::className(), 'targetAttribute' => ['usuario_id' => 'id']],
            [['cooperativa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cooperativa::className(), 'targetAttribute' => ['cooperativa_id' => 'id']],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'usuario_id' => Yii::t('app', 'Usuario ID'),
            'tamanyo' => Yii::t('app', 'Tamanyo'),
            'direccion' => Yii::t('app', 'Direccion'),
            'ciudad' => Yii::t('app', 'Ciudad'),
            'codigo_postal' => Yii::t('app', 'Codigo Postal'),
            'cooperativa_id' => Yii::t('app','Cooperativa ID'),
        ];
    }

    public function __toString()
    {
        return $this->direccion . ", " . $this->ciudad;
    }
    /**
     * Gets query for [[Usuario]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario()
    {
        return $this->hasOne(Usuario::className(), ['id' => 'usuario_id']);
    }

    /**
     * Gets query for [[ServicioCampos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getServicioCampos()
    {
        return $this->hasMany(ServicioCampo::className(), ['campo_id' => 'id']);
    }
    function beforeSave($insert) {
        if($this->isNewRecord){
            $this->id=count(Campo::find()->asArray()->all())+1;
        }  
        return parent::beforeSave($insert);
    }

    public function getPropietario(){
        return $this->usuario->apellidos . ", " .$this->usuario->nombre;
    }

    public function getDireccion_completa(){
        return $this->usuario->ciudad . ", " .$this->usuario->direccion;
    }
}
