<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gasto".
 *
 * @property int $id
 * @property int $servicio_id
 * @property string $concepto
 * @property string $descripcion
 * @property string $fecha
 * @property int $trabajador_id
 * @property int $total
 *
 * @property ServicioCampo $servicio
 * @property Trabajador $trabajador
 */
class Gasto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gasto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['servicio_id', 'concepto', 'descripcion', 'fecha', 'trabajador_id', 'total'], 'required'],
            [['servicio_id', 'trabajador_id', 'total'], 'integer'],
            [['fecha'], 'safe'],
            [['concepto'], 'string', 'max' => 60],
            [['descripcion'], 'string', 'max' => 200],
            [['servicio_id'], 'exist', 'skipOnError' => true, 'targetClass' => ServicioCampo::className(), 'targetAttribute' => ['servicio_id' => 'id']],
            [['trabajador_id'], 'exist', 'skipOnError' => true, 'targetClass' => Trabajador::className(), 'targetAttribute' => ['trabajador_id' => 'id']],
            [['cooperativa_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cooperativa::className(), 'targetAttribute' => ['cooperativa_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'servicio_id' => Yii::t('app', 'Servicio ID'),
            'concepto' => Yii::t('app', 'Concepto'),
            'descripcion' => Yii::t('app', 'Descripcion'),
            'fecha' => Yii::t('app', 'Fecha'),
            'trabajador_id' => Yii::t('app', 'Trabajador ID'),
            'total' => Yii::t('app', 'Total'),
            
        ];
    }

    /**
     * Gets query for [[Servicio]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getServicio()
    {
        return $this->hasOne(ServicioCampo::className(), ['id' => 'servicio_id']);
    }

    /**
     * Gets query for [[Trabajador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTrabajador()
    {
        return $this->hasOne(Trabajador::className(), ['id' => 'trabajador_id']);
    }

    function beforeSave($insert) {
        if($this->isNewRecord){
            $this->id=count(Gasto::find()->asArray()->all())+1;
        }  
        return parent::beforeSave($insert);
    }

    public function getNombreServicio(){
        return $this->servicio->nombreServicio;
    }

    public function getNombreTrabajador(){
        return $this->servicio->nombreTrabajador;
    }
}
