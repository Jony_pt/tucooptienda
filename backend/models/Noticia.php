<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "noticia".
 *
 * @property int $id
 * @property string $fecha_hora
 * @property string $titular
 * @property string $cuerpo
 * @property string $imagen_src
 */
class Noticia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'noticia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_hora', 'titular', 'cuerpo'], 'required'],
            [['imagen_src'], 'required', 'on' => 'create'],
            [['fecha_hora'], 'safe'],
            [['titular'], 'string', 'max' => 30],
            [['cuerpo'], 'string', 'max' => 1000],
            [['imagen_src'], 'string', 'max' => 60],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'fecha_hora' => Yii::t('app', 'Fecha Hora'),
            'titular' => Yii::t('app', 'Titular'),
            'cuerpo' => Yii::t('app', 'Cuerpo'),
            'imagen_src' => Yii::t('app', 'Imagen Src'),
        ];
    }

    function beforeSave($insert) {
        if($this->isNewRecord){
            $this->id=count(Noticia::find()->asArray()->all())+1;
        }  
        return parent::beforeSave($insert);
      }
}
