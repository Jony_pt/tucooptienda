<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProductoUsuario;

/**
 * ProductoUsuarioSearch represents the model behind the search form of `app\models\ProductoUsuario`.
 */
class ProductoUsuarioSearch extends ProductoUsuario
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'usuario_id', 'producto_id', 'temporada_id', 'cantidad', 'gasto', 'beneficio'], 'integer'],
            [['fecha'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductoUsuario::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'usuario_id' => $this->usuario_id,
            'producto_id' => $this->producto_id,
            'temporada_id' => $this->temporada_id,
            'cantidad' => $this->cantidad,
            'fecha' => $this->fecha,
            'gasto' => $this->gasto,
            'beneficio' => $this->beneficio,
        ]);

        return $dataProvider;
    }
}
