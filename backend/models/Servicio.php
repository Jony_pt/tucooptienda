<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "servicio".
 *
 * @property int $id
 * @property string $nombre
 * @property int $precio_metro
 * @property string|null $imagen_src
 *
 * @property ServicioCampo[] $servicioCampos
 */
class Servicio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'servicio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'precio_metro'], 'required'],
            [['imagen_src'], 'required', 'on' => 'create'],
            [['precio_metro'], 'integer'],
            [['nombre'], 'string', 'max' => 60],
            [['imagen_src'], 'string', 'max' => 28],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nombre' => Yii::t('app', 'Nombre'),
            'precio_metro' => Yii::t('app', 'Precio Metro'),
            'imagen_src' => Yii::t('app', 'Imagen Src'),
        ];
    }

    /**
     * Gets query for [[ServicioCampos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getServicioCampos()
    {
        return $this->hasMany(ServicioCampo::className(), ['servicio_id' => 'id']);
    }

    function beforeSave($insert) {
        if($this->isNewRecord){
            $this->id=count(Servicio::find()->asArray()->all())+1;
            //var_dump($this);
            //die();
        }  
        return parent::beforeSave($insert);
    }
}
