<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ServicioCampo;

/**
 * ServicioCampoSearch represents the model behind the search form of `app\models\ServicioCampo`.
 */
class ServicioCampoSearch extends ServicioCampo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'servicio_id', 'campo_id', 'temporada_id', 'gasto', 'beneficio', 'trabajador_id'], 'integer'],
            [['fecha_contrato', 'fecha_vencimiento'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ServicioCampo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'servicio_id' => $this->servicio_id,
            'campo_id' => $this->campo_id,
            'temporada_id' => $this->temporada_id,
            'gasto' => $this->gasto,
            'fecha_contrato' => $this->fecha_contrato,
            'fecha_vencimiento' => $this->fecha_vencimiento,
            'beneficio' => $this->beneficio,
            'trabajador_id' => $this->trabajador_id,
        ]);

        return $dataProvider;
    }
}
