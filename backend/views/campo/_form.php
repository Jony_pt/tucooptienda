<?php

use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Usuario;
use app\components\THtml;

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\typeahead\Typeahead;

/* @var $this yii\web\View */
/* @var $model app\models\Campo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="campo-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= THtml::autocomplete($model,'usuario_id',['usuario/lookup'], 'usuario');?>
    
    <?= $form->field($model, 'tamanyo')->textInput(['type' => 'number']) ?>

    <?= $form->field($model, 'direccion')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'ciudad')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'codigo_postal')->textInput(['maxlength' => 5]) ?>

    <div class="form-group">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div>
