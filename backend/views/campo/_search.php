<?php

use yii\helpers\Html;
use app\models\Usuario;
use app\components\THtml;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\CampoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="campo-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-md-3">
         <?= THtml::autocomplete($model,'usuario_id',['usuario/lookup'], 'usuario_id');?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'tamanyo') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'ciudad') ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Limpiar', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>