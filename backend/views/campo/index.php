<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CampoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Campos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="campo-index">

    <?php
    if (isset(Yii::$app->user->identity) && Yii::$app->user->identity->rol == "A") {//si el usuario logueado es admin, muestra el boton crear
    ?>
        <p>
        <p>
            <?= Html::a('Crear Campo', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php
    }
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [//las columnas que quieres que se vean
            ['class' => 'yii\grid\SerialColumn'],
            'propietario',
            'tamanyo',
            'direccion_completa',

            ['class' => 'yii\grid\ActionColumn'],//las opciones de ver/borrar/modificar
        ],
    ]) ?>

</div>