<?php

use yii\helpers\Html;
use app\models\Servicio;
use app\components\THtml;
use app\models\Trabajador;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\GastoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gasto-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="row">
        <div class="col-md-3">
            <?php $options = ArrayHelper::map(Servicio::find()->asArray()->all(), 'id', 'nombre');
            echo $form->field($model, 'servicio_id')->dropDownList($options, ['prompt' => 'Seleccione servicio...']);
            ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'fecha') ?>
        </div>
        <div class="col-md-3">
            <?= THtml::autocomplete($model, 'trabajador_id', ['trabajador/lookup'], 'trabajador_id'); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'total') ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Limpiar', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>