<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GastoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Gastos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gasto-index">

    <?php
    if (isset(Yii::$app->user->identity) && Yii::$app->user->identity->rol == "A") {
    ?>
        <p>
            <?= Html::a(Yii::t('app', 'Crear Gasto'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php
    }
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nombreServicio',
            'concepto',
            'descripcion',
            'fecha',
            'nombreTrabajador',
            'total',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]) ?>

</div>