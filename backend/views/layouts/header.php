<?php

use yii\helpers\Url;
use yii\helpers\Html;
?>
<header class="main-header">
  <!-- Logo -->
  <a href="index.html" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>TuCoop</b></span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><img style="width: 150px; height:40px" src="http://alum2.iesfsl.org:81/imagenes/logo.png" alt=""></span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <?php
        if (Yii::$app->user->identity) {
        ?>
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <?= Html::img('http://alum2.iesfsl.org:81/imagenes/perfil/'.Yii::$app->user->identity->imagen_src, ['class' => 'user-image', 'alt' => 'User Image']) 
              ?>
              <span class="hidden-xs"><?= Yii::$app->user->identity->nombre?></span>
              
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <?= Html::img('http://alum2.iesfsl.org:81/imagenes/perfil/'.Yii::$app->user->identity->imagen_src, ['class' => 'img-circle', 'alt' => 'User Image']) ?>
                <p>
                <?= Yii::$app->user->identity->apellidos . ", " . Yii::$app->user->identity->nombre?>
                <?="<br>". Yii::$app->user->identity->mail?>
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right">
                  <?php
                  echo '<a>'
                    . Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                      'Logout',
                      ['class' => 'btn logout']
                    )
                    . Html::endForm()
                    . '</a>';
                    
                  ?>
                </div>
              </li>
            </ul>
          </li>
        <?php
        } else {
          echo Html::a("Entrar", ["/site/login"], ['class' => "btn btn-default btn-flat" , "style" => 'margin-top:10px; margin-right:10px']);
        } ?>
      </ul>
    </div>
  </nav>
</header>