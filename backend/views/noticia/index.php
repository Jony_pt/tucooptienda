<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NoticiaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Noticias');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="noticia-index">

    <?php
    if (isset(Yii::$app->user->identity) && Yii::$app->user->identity->rol == "A") {
    ?>
        <p>
            <?= Html::a(Yii::t('app', 'Crear Noticia'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php
    }
    ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'fecha_hora',
            'titular',
            'cuerpo',
            [
                'attribute' => 'imagen_src',
                'label' => 'Image',
                'format' => 'html',
                'content' => function ($model) {
                    $url = "http://alum2.iesfsl.org:81/imagenes/noticias/" . $model->imagen_src;
                    return Html::img($url, ['alt' => 'yii', 'width' => '125', 'height' => '125', 'class'=>'img-fluid']);
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]) ?>

</div>