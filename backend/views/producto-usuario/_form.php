<?php

use yii\helpers\Html;
use app\models\Usuario;
use app\models\Producto;
use app\components\THtml;
use app\models\Temporada;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\ProductoUsuario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="producto-usuario-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= THtml::autocomplete($model,'usuario_id', ['usuario/lookup'], 'usuario');?>

    <?php $options = ArrayHelper::map(Producto::find()->asArray()->all(), 'id', 'nombre');
    echo $form->field($model, 'producto_id')->dropDownList($options, ['prompt' => 'Seleccione producto...']);
    ?>

    <?php $options = ArrayHelper::map(Temporada::find()->asArray()->all(), 'id', 'nombre');
    echo $form->field($model, 'temporada_id')->dropDownList($options, ['prompt' => 'Seleccione temporada...']);
    ?>
    
    <?= $form->field($model, 'cantidad')->textInput(['type' => 'number']) ?>

    <?= $form->field($model, 'fecha')->widget(\yii\jui\DatePicker::classname(), [
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>

    <?= $form->field($model, 'gasto')->textInput(['type' => 'number']) ?>

    <?= $form->field($model, 'beneficio')->textInput(['type' => 'number']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>