<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\Widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Productos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="producto-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php
    if (isset(Yii::$app->user->identity) && Yii::$app->user->identity->rol == "A") {
    ?>
    <p>
        <?= Html::a(Yii::t('app', 'Crear Producto'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php
    }
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nombre',
            
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]) ?>


</div>
