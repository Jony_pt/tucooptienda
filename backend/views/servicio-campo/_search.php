<?php

use app\models\Campo;
use yii\helpers\Html;
use app\models\Usuario;
use app\models\Producto;
use app\models\Servicio;
use app\components\THtml;
use app\models\Temporada;
use app\models\Trabajador;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\ServicioCampoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="servicio-campo-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class='row'>
        <div class='col-md-2'>
            <?php $options = ArrayHelper::map(Servicio::find()->asArray()->all(), 'id', 'nombre');
            echo $form->field($model, 'servicio_id')->dropDownList($options, ['prompt' => 'Seleccione servicio...']);
            ?>
        </div>
        <div class='col-md-2'>
            <?= THtml::autocomplete($model, 'campo_id', ['campo/lookup'], 'campo_id'); ?>
        </div>
        <div class='col-md-2'>
            <?php $options = ArrayHelper::map(Temporada::find()->asArray()->all(), 'id', 'nombre');
            echo $form->field($model, 'temporada_id')->dropDownList($options, ['prompt' => 'Seleccione temporada...']);
            ?>
        </div>
        <div class='col-md-2'>
            <?= THtml::autocomplete($model, 'trabajador_id', ['trabajador/lookup'], 'trabajador_id'); ?>
        </div>
        <div class='col-md-2'>
            <?= $form->field($model, 'fecha_contrato') ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Buscar', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Limpiar', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>