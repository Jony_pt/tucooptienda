<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ServicioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Servicios');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="servicio-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php
    if (isset(Yii::$app->user->identity) && Yii::$app->user->identity->rol == "A") {
    ?>

        <p>
            <?= Html::a(Yii::t('app', 'Crear Servicio'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php
    }
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nombre',
            'precio_metro',
            [
                'attribute' => 'imagen_src',
                'label' => 'Image',
                'format' => 'html',
                'content' => function ($model) {
                    $url = "http://alum2.iesfsl.org:81/imagenes/servicios/" . $model->imagen_src;
                    return Html::img($url, ['alt' => 'yii', 'width' => '125', 'height' => '125', 'class'=>'img-thumbnail']);
                }
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]) ?>


</div>