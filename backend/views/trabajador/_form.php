<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Trabajador */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="trabajador-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'apellidos')->textInput(['maxlength' => 60]) ?>

    <?= $form->field($model, 'dni')->textInput(['maxlength' => 9]) ?>

    <?= $form->field($model, 'mail')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => 9]) ?>

    <?= $form->field($model, 'direccion')->textInput(['maxlength' => 60]) ?>

    <?= $form->field($model, 'ciudad')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'codigo_postal')->textInput(['maxlength' => 5]) ?>

    <?= $form->field($model, 'IBAN')->textInput(['maxlength' => 26]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
