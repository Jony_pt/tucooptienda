<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use app\models\ServicioCampo;
use yii\data\ActiveDataProvider;
use app\models\ServicioCampoSearch;

/* @var $this yii\web\View */
/* @var $model app\models\Trabajador */

$this->title = $model->apellidos . ", " . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Trabajadores'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="trabajador-view">

    <br>

    <p>
        <?= Html::a(Yii::t('app', 'Modificar'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Eliminar'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', '¿Está seguro de eliminar este elemento?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'apellidos',
            'dni',
            'mail',
            'telefono',
            'direccion',
            'ciudad',
            'codigo_postal',
            'IBAN',
        ],
    ]) ?>

    <?php

    $trabajos = ServicioCampo::find()->where("trabajador_id=" . $model->id);

    $dataProvider = new ActiveDataProvider([
        'query' => $trabajos,
    ]);

    echo "<br> <h3>TRABAJOS REALIZADOS</h3>";
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nombreServicio',
            'direccion',
            'fecha_contrato',
        ],
    ]) ?>

</div>