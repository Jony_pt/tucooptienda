<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\password\PasswordInput;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usuario-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => 60]) ?>

    <?= $form->field($model, 'apellidos')->textInput(['maxlength' => 60]) ?>

    <?= $form->field($model, 'mail')->textInput(['maxlength' => 60]) ?>

    <?= $form->field($model, 'direccion')->textInput(['maxlength' => 60]) ?>

    <?= $form->field($model, 'ciudad')->textInput(['maxlength' => 60]) ?>

    <?= $form->field($model, 'codigo_postal')->textInput(['maxlength' => 5]) ?>

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => 9]) ?>

    <?= $form->field($model, 'dni')->textInput(['maxlength' => 9]) ?>

    <?php
    echo $form->field($model,'rol')->dropDownList(['A'=>'Administrador','U'=>'Usuario']);
    ?>

    <?= $form->field($model, 'contrasenya')->widget(PasswordInput::classname(), []); ?>
    <?php echo '<p> Para mantener la contraseña deje el campo tal cual. si quiere cambiarla ponga contraeá</p>'?>

    <?= $form->field($model, 'cuota')->textInput(['type' => 'number']) ?>

    <?= $form->field($model, 'IBAN')->textInput(['maxlength' => 60]) ?>

    <?= $form->field($model, 'imagen_src')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
