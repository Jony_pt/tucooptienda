<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsuarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Usuarios');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    if (isset(Yii::$app->user->identity) && Yii::$app->user->identity->rol == "A") {
    ?>
        <p>
            <?= Html::a(Yii::t('app', 'Crear Usuario'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php
    }
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nombre',
            'apellidos',
            'dni',
            'mail',
            'telefono',
            'rolUsuario',
            [
                'attribute' => 'imagen_src',
                'label' => 'Image',
                'format' => 'html',
                'content' => function ($model) {
                    $url = "http://alum2.iesfsl.org:81/imagenes/perfil/" . $model->imagen_src;
                    return Html::img($url, ['alt' => 'yii', 'width' => '125', 'height' => '150', 'class'=>'img-thumbnail']);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]) ?>

</div>