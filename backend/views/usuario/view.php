<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\DetailView;
use app\models\ProductoUsuario;
use yii\data\ActiveDataProvider;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */

$this->title = $model->apellidos . ", " . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Usuarios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="usuario-view">

    <p>
        <?= Html::a(Yii::t('app', 'Modificar'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Eliminar'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', '¿Está seguro de eliminar este elemento?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'apellidos',
            'mail',
            'direccion',
            'ciudad',
            'codigo_postal',
            'telefono',
            'dni',
            'rolUsuario',
            'cuota',
            'IBAN',
        ],
    ]);
    
    $ruta = "http://alum2.iesfsl.org:81/imagenes/perfil/" .$model->imagen_src;//para mostrar la imagen de perfil
    echo "<img src='$ruta' style='width:25%;height:25%;'>" ;?>
    <br>
    <?php

    $entregas = ProductoUsuario::find()->where("usuario_id=" . $model->id);//cojo todas las entregas del user seleccinado

    $dataProvider = new ActiveDataProvider([//lo convertimos la consulta en un objeto dataprovider para poder mostrarlo en el gridview
        'query' => $entregas,
    ]);

    echo "<br> <h3>ENTREGA REALIZADAS</h3>";
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'nombreProducto',
            'temporada_id',
            'cantidad',
            'fecha',
        ],
    ]) 
    ?>

</div>