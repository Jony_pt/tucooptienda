<?php

use app\models\Producto;
use yii\helpers\Html;
use app\models\Temporada;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\Venta */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="venta-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fecha')->widget(\yii\jui\DatePicker::classname(), [
        'dateFormat' => 'yyyy-MM-dd',
    ]) ?>

    <?= $form->field($model, 'precio')->textInput(['type' => 'number']) ?>

    <?= $form->field($model, 'comprador')->textInput(['maxlength' => 60]) ?>

    <?= $form->field($model, 'cantidad')->textInput(['type' => 'number']) ?>

    <?php
    $options = ArrayHelper::map(Producto::find()->asArray()->all(), 'id', 'nombre');
    echo $form->field($model, 'producto_id')->dropDownList($options, ['prompt' => 'Seleccione producto...']);
    ?>

    <?php
    $options = ArrayHelper::map(Temporada::find()->asArray()->all(), 'id', 'nombre');
    echo $form->field($model, 'temporada_id')->dropDownList($options, ['prompt' => 'Seleccione temporada...']);
    ?>

    
    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>