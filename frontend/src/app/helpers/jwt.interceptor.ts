import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AccountService } from '../account/account-service.service';
@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private accountService: AccountService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add auth header with jwt if user is logged in and request is to the api url
        const user = this.accountService.userValue;
        const isLoggedIn = user && user.token;
        
        if (isLoggedIn) {
            //meter token en el header
            //if (request.url != "http://localhost/tucoop/api/web/noticias" && request.url != "http://localhost/tucoop/api/web/servicios") {
                console.log("1 "+ request.headers.get('Authorization'));
                /*request = request.clone({
                    setHeaders: {
                      Authorization: `Bearer ${user.token}`
                    }
                  });
            */
            //request = request.clone({ headers: new HttpHeaders({'Authorization': 'Bearer ' + user.token })});
            //}
        }
        console.log("Autorizacion: " + request.headers.get('Authorization'));
        console.log(request.headers);
        console.log(request);
        return next.handle(request);
    }
}