import { Component, OnInit } from '@angular/core';
import {AccountService} from '../account/account-service.service';
@Component({
  selector: 'app-menu2',
  templateUrl: './menu2.component.html',
  styleUrls: ['./menu2.component.scss']
})
export class Menu2Component implements OnInit {

  constructor(private accountService: AccountService,) { }
  usuario: any = {};
  ngOnInit(): void {
    this.accountService.user.subscribe(
      data => {
        this.usuario = data;
      }
    );
  }
  
  onLogout(){
    this.accountService.logout();
  }
}
