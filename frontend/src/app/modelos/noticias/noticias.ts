
export interface Noticias {
    id: any;
    fecha_hora: string;
    titular: string;
    cuerpo: string;
    imagen_src: string;
}
