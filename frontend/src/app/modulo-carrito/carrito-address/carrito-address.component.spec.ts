import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CarritoAddressComponent } from './carrito-address.component';

describe('CarritoAddressComponent', () => {
  let component: CarritoAddressComponent;
  let fixture: ComponentFixture<CarritoAddressComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarritoAddressComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarritoAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
