import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from '../../account/account-service.service';

@Component({
  selector: 'app-carrito-address',
  templateUrl: './carrito-address.component.html',
  styleUrls: ['./carrito-address.component.scss']
})
export class CarritoAddressComponent implements OnInit {

  constructor(private router: Router, private accountServie: AccountService) { }
  usuario: any;
  usuarios: any;
  
  ngOnInit(): void {

    this.accountServie.user.subscribe(
      data=>{
        this.usuario = data;

        this.usuarios = Object(this.usuario);

      }
    )
  }

  public realizarPago() {
   
         this.router.navigate(['carritoPayment']);

        }
}
