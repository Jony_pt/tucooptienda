import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ArticulosService } from '../modulo-tienda/articulos.service';

@Injectable({
  providedIn: 'root'
})
export class CarritoService {
  private carrito: BehaviorSubject<Array<any>>; //Definimos nuestro BehaviorSubject, este debe tener un valor inicial siempre
  public articulos: Observable<Array<any>>;   //Tenemos un observable con el valor actual del BehaviourSubject
  public cantidad_total: any[] = [];

  constructor(private articulosService: ArticulosService) {
    this.carrito = new BehaviorSubject<Array<any>>(JSON.parse(localStorage.getItem('prueba') || '[]'));
    this.articulos = this.carrito.asObservable();
  }

  public changeCart(newData: any, totalproducts: number) {
    //Obtenemos el valor actual
    let listCart = this.carrito.getValue();
    //Si no es el primer item del carrito
    if (listCart) {
      //Buscamos si ya cargamos ese item en el carrito
      let objIndex = listCart.findIndex(obj => obj.id == newData.id);
      //Si ya cargamos uno aumentamos su cantidad
      if (objIndex != -1) {
        console.log("cantidad",listCart[objIndex].cantidad);
        listCart[objIndex].cantidad += totalproducts;
        console.log("cantidad2",listCart[objIndex].cantidad);
        this.sessionId(listCart);
      }
      //Si es el primer item de ese tipo lo agregamos derecho al carrito
      else {
        console.log("newData1", newData.cantidad);
        newData.cantidad = totalproducts;
        console.log("newdata2", newData.cantidad);
        console.log("objeto",newData);
        listCart.push(newData);
        this.sessionId(listCart);
      }
    }
    //Si es el primer elemento lo inicializamos
    else {
      listCart = [];
      listCart.push(newData);
      this.sessionId(newData);
    }
    this.carrito.next(listCart); //Enviamos el valor a todos los Observers que estan escuchando nuestro Observable
  }

  public sessionId(value: any) {
    localStorage.setItem('prueba', JSON.stringify(value));
  }

  public getsessionId() {
    let datosCarrito = JSON.parse(localStorage.getItem('prueba') || '[]');
    return datosCarrito;
  }

  public removeArticulo(articulo: any) {
    let datos: any[] = [];
    let datosCarrito = JSON.parse(localStorage.getItem('prueba') || '[]');
    localStorage.removeItem('prueba');
    for (let i = 0; datosCarrito.length; i++) {
      if (datosCarrito[i].id != articulo.id) {
        datos.push(datosCarrito[i]);
        localStorage.setItem('prueba', JSON.stringify(datos));
      }
    }
    this.removeElementCart(articulo);
  }

  public removeArticulos() {
    localStorage.removeItem('prueba');
  }

  public removeElementCart(newData: any) {
    //Obtenemos el valor actual de carrito
    let listCart = this.carrito.getValue();
    //Buscamos el item del carrito para eliminar
    let objIndex = listCart.findIndex((obj => obj.id == newData.id));
    if (objIndex != -1) {
      //Seteamos la cantidad en 1 (ya que los array se modifican los valores por referencia, si vovlemos a agregarlo la cantidad no se reiniciará)
      listCart[objIndex].cantidad = 1;
      //Eliminamos el item del array del carrito
      listCart.splice(objIndex, 1);
    }
    this.carrito.next(listCart); //Enviamos el valor a todos los Observers que estan escuchando nuestro Observable
  }
}
