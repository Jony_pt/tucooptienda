import { Component, OnInit } from '@angular/core';
import { CarritoService } from '../carrito.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { items } from '../../models/items';
import { ArticulosService } from '../../modulo-tienda/articulos.service';
//import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AccountService } from '../../account/account-service.service';
//import { ModalCompraComponent } from '../modal-compra/modal-compra.component';
import { ThrowStmt } from '@angular/compiler';



@Component({
  selector: 'app-carrito',
  templateUrl: './carrito.component.html',
  styleUrls: ['./carrito.component.scss']
})
export class CarritoComponent implements OnInit {
  public items!: items[];
  public totalPrice = 0;
  public IVA = 0;
  public totalQuantity = 0;
  public factura: any[] = [];
  public total = 0;
  public totales = 0;
  public TotalIva: any;
  public TotalProduct: any;
  

  constructor(private carritoService: CarritoService, private articuloService: ArticulosService, private accountService: AccountService,  private router: Router) {
  }

  ngOnInit() {
    this.carritoService.articulos.subscribe(
      data => {
        this.items = data;
        //hacer el IVA
        for (let i = 0; i < this.items.length; i++) {
          this.total = this.items[i].cantidad * this.items[i].precio;
          this.totales = this.totales + this.total;
          this.IVA = this.totales * 0.04;
          console.log("IVA", this.IVA);
          this.totales = this.totales + this.IVA;
          this.TotalIva = this.totales.toFixed(2);
          this.TotalProduct = this.total.toFixed(2);
          console.log("thistotales", this.totales);
          console.log("thisiva", this.IVA)
          console.log("totalproducto", this.TotalProduct);
          console.log("totales", this.TotalIva);
        }

      }
    )
  }

  public remove(producto: any) {
    if (this.items.length == 1) {
      window.location.reload();
      this.carritoService.removeArticulos();
    } else {
      window.location.reload();
      this.carritoService.removeArticulo(producto);
    }
  }

  public removeArticulos() {
    window.location.reload();
    this.carritoService.removeArticulos();
  }

  public realizarPedido() {
    let usuario: any, usuario_id: any;
    this.accountService.user.subscribe(
      data => {
        usuario = data;
        usuario_id = Object(usuario);
        if (Object.keys(usuario).length === 0) {
          this.router.navigate(['login']);
        } else {
         this.router.navigate(['carritoAddress']);

        }
      }
    );

  }
}