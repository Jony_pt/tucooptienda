import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CarritoComponent } from './carrito/carrito.component';
import { CarritoAddressComponent } from './carrito-address/carrito-address.component';
import { CarritoPaymentComponent } from './carrito-payment/carrito-payment.component';
import { DxAutocompleteModule } from 'devextreme-angular';
import { DxTextBoxModule } from 'devextreme-angular';



@NgModule({
  declarations: [CarritoComponent, CarritoAddressComponent, CarritoPaymentComponent],
  imports: [
    CommonModule,
    DxAutocompleteModule,
    FormsModule,
    DxTextBoxModule
  ]
})
export class ModuloCarritoModule { }
