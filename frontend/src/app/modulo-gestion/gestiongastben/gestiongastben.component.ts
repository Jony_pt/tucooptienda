import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label, Color, MultiDataSet } from 'ng2-charts';
import { ServiceGastosService } from '../../modulo-gestion/service-gastos.service';
import { ServiceBeneficiosService } from '../../modulo-gestion/service-beneficios.service';
import { ServiceServiciosService } from '../../modulo-gestion/service-servicios.service';

@Component({
  selector: 'app-gestiongastben',
  templateUrl: './gestiongastben.component.html',
  styleUrls: ['./gestiongastben.component.scss']
})
export class GestiongastbenComponent implements OnInit {
  servicios: any = [];
  beneficios: any = [];
  gastos: any = [];
  select: any;
  datosben: any = [];
  datosgast: any = [];
  temporadas: any[] = [];
  campos: any[] = [];
  datosServ: any[] = [];
  select2:any;
  constructor(private ServiceServiciosService: ServiceServiciosService, private ServiceBeneficiosService: ServiceBeneficiosService, private ServiceGastosService: ServiceGastosService) { }

  ngOnInit(): void {
    var ben: String | any[] = [];
    var gast: String | any[] = [];
    var serv: String | any[] = [];
    
    //servicios para los gastos
    this.ServiceServiciosService.getServicios()
      .subscribe(
        data => {
          this.servicios = data;
          serv = Object(this.servicios);
          
          for (let index = 0; index < serv.length; index++) {
            //METO LOS DATOS DE LA TEMPORADA 1 EN LAS GRAFICAS
            if (serv[index]["campo_nombre"] == serv[0]["campo_nombre"]) {
              this.datosServ.push(serv[index]);
            }
          }
          console.log(this.datosServ);
          //RECORRO LOS CAMPOS PARA HACER EL SELECT
          for (let index = 0; index < serv.length; index++) {
            if (!this.campos.includes(serv[index]["campo_nombre"])) {
              this.campos.push(serv[index]["campo_nombre"]);
            }
          }
          this.select2 = document.getElementById("campos");
          this.select2.addEventListener('change',
            () => {
              var camp = ""

              var selectedOption = this.select2.options[this.select2.selectedIndex];
              camp = selectedOption.value;

              this.datosServ.splice(0, this.datosServ.length);

              for (let index = 0; index < serv.length; index++) {
                if (serv[index]["campo_nombre"] == camp) {
                  this.datosServ.push(serv[index]);
                }
              }

            });
        },
        error => {
          console.log(error)
        }
      );

    //beneficios
    this.ServiceBeneficiosService.getNoticias()
      .subscribe(
        data => {
          //cogemos los datos de la api
          this.beneficios = data;
          //convierto a array el json
          ben = Object(this.beneficios);
          //primera ejec
          for (let index = 0; index < ben.length; index++) {
            //METO LOS DATOS DE LA TEMPORADA 1 EN LAS GRAFICAS
            if (ben[index]["temporada_id"] == 1) {
              this.datosben.push(ben[index]["beneficio"]);
              this.benbarChartLabels.push(ben[index]["fecha"]);
            }
          }
          //RECORRO LAS TEMPORADAS PARA HACER EL SELECT
          for (let index = 0; index < ben.length; index++) {
            if (!this.temporadas.includes(ben[index]["temporada_id"])) {
              this.temporadas.push(ben[index]["temporada_id"]);
            }
          }
          this.select = document.getElementById("temporadas");
          this.select.addEventListener('change',
            () => {
              var temp = 0

              var selectedOption = this.select.options[this.select.selectedIndex];
              temp = selectedOption.value;

              this.datosben.splice(0, this.datosben.length);
              this.benbarChartLabels = [];

              for (let index = 0; index < ben.length; index++) {
                if (ben[index]["temporada_id"] == temp) {
                  this.datosben.push(ben[index]["beneficio"]);
                  this.benbarChartLabels.push(ben[index]["fecha"]);
                }
              }

            });
        },
        error => {
          console.log(error)
        });
   
    //gastos
    this.ServiceGastosService.getNoticias()
      .subscribe(
        data => {
          this.gastos = data;

          gast = Object(this.gastos);
          for (let index = 0; index < gast.length; index++) {
            if (gast[index]["temporada_id"] == 1) {
              console.log(gast);
              this.datosgast.push(gast[index]["gasto"] * -1);
              this.gastbarChartLabels.push(gast[index]["fecha_contrato"]);
            }
          }

          for (let index = 0; index < gast.length; index++) {
            if (!this.temporadas.includes(gast[index]["temporada_id"])) {
              this.temporadas.push(gast[index]["temporada_id"]);
            }
          }

          this.select = document.getElementById("temporadas");
          this.select.addEventListener('change',
            () => {
              var temp = 0

              var selectedOption = this.select.options[this.select.selectedIndex];
              temp = selectedOption.value;

              this.datosgast.splice(0, this.datosgast.length);
              this.gastbarChartLabels = [];

              for (let index = 0; index < gast.length; index++) {
                if (gast[index]["temporada_id"] == temp) {
                  this.datosgast.push(gast[index]["gasto"] * -1);
                  this.gastbarChartLabels.push(gast[index]["fecha_contrato"]);
                }
              }

            });
        },
        error => {
          console.log(error)
        });
  }

  //beneficios 
  benbarChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          max: 1000,
          min: 0
        }
      }]
    }
  };
  benbarChartLabels: Label[] = [];
  benbarChartType: ChartType = 'bar';
  benbarChartLegend = true;
  benbarChartPlugins = [];
  benlineChartColors: Color[] = [
    {
      backgroundColor: 'green'

    }];
  benbarChartData: ChartDataSets[] = [
    { data: this.datosben, label: 'beneficios' }

  ];
  //gastos
  gastbarChartType: ChartType = 'bar';
  gastbarChartLegend = true;
  gastbarChartPlugins = [];

  gastlineChartColors: Color[] = [
    {
      backgroundColor: 'red'

    }];
  gastbarChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          max: 0,
          min: -1000
        }
      }]
    }
  };
  gastbarChartLabels: Label[] = [];
  gastbarChartData: ChartDataSets[] = [
    { data: this.datosgast, label: 'Gastos' }

  ];
}






