import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionhomeComponent } from './gestionhome.component';

describe('GestionhomeComponent', () => {
  let component: GestionhomeComponent;
  let fixture: ComponentFixture<GestionhomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionhomeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionhomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
