import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label, Color, MultiDataSet } from 'ng2-charts';
import { ServiceGastosService } from '../../modulo-gestion/service-gastos.service';
import { ServiceBeneficiosService } from '../../modulo-gestion/service-beneficios.service';
import { ServiceProductosService } from '../../modulo-gestion/service-productos.service';

@Component({
  selector: 'app-gestionhome',
  templateUrl: './gestionhome.component.html',
  styleUrls: ['./gestionhome.component.scss']
})
export class GestionhomeComponent implements OnInit {
  datosben: any = [];
  datosgast: any = [];
  datosprod: any = [];
  beneficios: any = [];
  gastos: any = [];
  max: any;
  productos: any = [];
  temporadas: any[] = [];
  constructor(private ServiceGastosService: ServiceGastosService, private ServiceBeneficiosService: ServiceBeneficiosService, private ServiceProductosService: ServiceProductosService) { }
  ngOnInit(): void {
    var pro: String | any[] = [];
    var ben: String | any[] = [];
    var gast: String | any[] = [];
    this.ServiceBeneficiosService.getNoticias()
      .subscribe(
        data => {
          //cogemos los datos de la api
          this.beneficios = data;
          //convierto a array el json
          ben = Object(this.beneficios);
          //Cogemos la ultima posición de el array donde se encuentra la ultima temporada
          this.max=ben[ben.length-1];            
          for (let index = 0; index < ben.length; index++) {
            //METO LOS DATOS DE LA TEMPORADA 1 EN LAS GRAFICAS

            if (ben[index]["temporada_id"] == this.max["temporada_id"]) {
              this.datosben.push(ben[index]["beneficio"]);
              this.benBarChartLabels.push(ben[index]["fecha"]);
            }
          }
        });
    //gastos
    this.ServiceGastosService.getNoticias()
      .subscribe(
        data => {
          this.gastos = data;

          gast = Object(this.gastos);
          
          this.max=gast[gast.length-1];            
          for (let index = 0; index < gast.length; index++) {
            if (gast[index]["temporada_id"] == this.max["temporada_id"]) {
              this.datosgast.push(gast[index]["gasto"] * -1);
              this.gastBarChartLabels.push(gast[index]["fecha_contrato"]);
            }
          }

        });

    //Productos
    this.ServiceProductosService.getNoticias()
      .subscribe(
        data => {
          //COJEMOS LOS DATOS DE LA API
          this.productos = data;
          //CONVIERTO A ARRAY EL JSON
          pro = Object(this.productos);
          //Cogemos la ultima temporada
          this.max=pro[pro.length-1];            
          //PRIEMRA EJECUCIÓN
          for (let index = 0; index < pro.length; index++) {
            //METO LOS DATOS DE LA ULTIMA TEMPORADA EN LAS GRAFICAS
            if (pro[index]["temporada_id"] >= this.max["temporada_id"]) {
              this.datosprod.push(pro[index]["kilos"]);
              this.prodBarChartLabels.push(pro[index]["producto_nombre"]);
            }
          }
        });
  }
  //SE DECLARAN LAS VARIABLES GRAFICA PRODUCTOS
  prodBarChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          max: 1000,
          min: 0
        }
      }]
    }
  };
  prodBarChartLabels: Label[] = [];
  prodBarChartType: ChartType = 'bar';
  prodBarChartLegend = true;
  prodBarChartPlugins = [];
  prodLineChartColors: Color[] = [
    {
      backgroundColor: 'orange',
    },
  ];
  prodBarChartData: ChartDataSets[] = [
    { data: this.datosprod, label: 'Kilos' }
  ];


  //SE DECLARAN LAS VARIABLES GRAFICA BENEFICIOS
  benBarChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          max: 1000,
          min: 0
        }
      }]
    }
  };
  benBarChartLabels: Label[] = [];
  benBarChartType: ChartType = 'bar';
  benBarChartLegend = true;
  benBarChartPlugins = [];
  benLineChartColors: Color[] = [
    {
      backgroundColor: 'green'

    },
    { backgroundColor: 'red' },
  ];
  benBarChartData: ChartDataSets[] = [
    { data: this.datosben, label: 'Beneficios' },

  ];
  //grafica gastos

  gastBarChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          min: -1000,
          max:0,
        }
      }]
    }
  };
  gastBarChartLabels: Label[] = [];
  gastBarChartType: ChartType = 'bar';
  gastBarChartLegend = true;
  gastBarChartPlugins = [];
  gastLineChartColors: Color[] = [
    {
      backgroundColor: 'red'
    },
  ];
  gastBarChartData: ChartDataSets[] = [
    { data: this.datosgast, label: 'Gastos' }

  ];
}