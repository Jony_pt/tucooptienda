import { Component, OnInit } from '@angular/core';
import { ServiceServiciosService } from '../../modulo-gestion/service-servicios.service';

@Component({
  selector: 'app-gestionservicios',
  templateUrl: './gestionservicios.component.html',
  styleUrls: ['./gestionservicios.component.scss']
})
export class GestionserviciosComponent implements OnInit {

  constructor(private ServiceServiciosService: ServiceServiciosService) { }
  servicios: any[] = [];
  servicios2: any[] = [];
  esta: boolean = false;
  ngOnInit(): void {
    this.ServiceServiciosService.getServicios()
      .subscribe(
        data => {
          this.servicios = data;
          console.log(this.servicios);

          this.servicios.sort(function (a, b) {
            if (a.servicio_nombre > b.servicio_nombre) {
              return 1;
            }
            if (a.servicio_nombre < b.servicio_nombre) {
              return -1;
            }
            // a must be equal to b
            return 0;
          });

          var flag = this.servicios[0].servicio_nombre;
          this.servicios2.push(this.servicios[0]);
          let i = 1;

          while (i < this.servicios.length) {
            if (flag != this.servicios[i]["servicio_nombre"]) {
              this.servicios2.push(this.servicios[i]);
              flag = this.servicios[i].servicio_nombre;
            }
            i++;
          }
          console.log(this.servicios2);

        },
        error => {
          console.log(error)
        }
      );


  }
}
