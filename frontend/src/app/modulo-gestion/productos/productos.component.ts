import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Label, Color, MultiDataSet } from 'ng2-charts';
import { ServiceProductosService } from '../../modulo-gestion/service-productos.service';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss']
})
export class ProductosComponent implements OnInit {
  productos: any[] = [];
  datos: any = [];
  select: any;
  temporadas: any[] = [];

  datos2: any = [];
  select2: any;
  temporadas2: any[] = [];
  constructor(private ServiceProductosService: ServiceProductosService) { }


  ngOnInit(): void {
    var pro: string | any[] = [];
    this.ServiceProductosService.getNoticias()
      .subscribe(
        data => {
          //COJEMOS LOS DATOS DE LA API
          this.productos = data;
          
          //CONVIERTO A ARRAY EL JSON
          pro = Object(this.productos);
          //PRIMERA EJECUCIÓN GRAFICA 1
          for (let index = 0; index < pro.length; index++) {
            //METO LOS DATOS DE LA TEMPORADA 1 EN LAS GRAFICAS
            if (pro[index]["temporada_id"] == 1) {
              this.datos.push(pro[index]["kilos"]);
              this.barChartLabels.push(pro[index]["producto_nombre"]);
            }
          }
          //RECORRO LAS TEMPORADAS PARA HACER EL SELECT
          for (let index = 0; index < pro.length; index++) {
            if (!this.temporadas.includes(pro[index]["temporada_id"])) {
              this.temporadas.push(pro[index]["temporada_id"]);
            }
          }
          
          //PRIMERA EJECUCION GRAFICA 2
          var primero = pro[0]["producto_nombre"];
          for (let index = 0; index < pro.length; index++) {
            //METO LOS DATOS DE LA TEMPORADA 1 EN LAS GRAFICAS
            if (pro[index]["producto_nombre"] == primero) {
              this.datos2.push(pro[index]["kilos"]);
              this.barChartLabels2.push("Temporada " + pro[index]["temporada_id"]);
            }
          }
          //RECORRO LOS PRODUCTOS PARA HACER EL SELECT
          for (let index = 0; index < pro.length; index++) {
            if (!this.temporadas2.includes(pro[index]["producto_nombre"])) {
              this.temporadas2.push(pro[index]["producto_nombre"]);
            }
          }


          //ON CHANGE
          this.select = document.getElementById("temporada");
          this.select.addEventListener('change',
            () => {
              var temp = 0;
              //SE SELECCIONA LA TEMPORADA EN EL SELECT
              var selectedOption = this.select.options[this.select.selectedIndex];
              temp = selectedOption.value;
              //SE VACIAN LOS DATOS ANTERIORES
              this.datos.splice(0, this.datos.length);
              this.barChartLabels = [];
              //SE METEN LOS DATOS EN LAS GRAFICAS DE LA TEMPORADA SELECCIONADA
              for (let index = 0; index < pro.length; index++) {
                if (pro[index]["temporada_id"] == temp) {
                  this.datos.push(pro[index]["kilos"]);
                  this.barChartLabels.push(pro[index]["producto_nombre"]);
                }
              }
            });

            this.select2 = document.getElementById("temporada2");
            this.select2.addEventListener('change',
            () => {
              var temp = pro[0]["producto_nombre"];
              //SE SELECCIONA LA TEMPORADA EN EL SELECT
              var selectedOption = this.select2.options[this.select2.selectedIndex];
              temp = selectedOption.value;
              //SE VACIAN LOS DATOS ANTERIORES
              this.datos2.splice(0, this.datos2.length);
              this.barChartLabels2 = [];
              //SE METEN LOS DATOS EN LAS GRAFICAS DE LA TEMPORADA SELECCIONADA
              for (let index = 0; index < pro.length; index++) {
                if (pro[index]["producto_nombre"] == temp) {
                  this.datos2.push(pro[index]["kilos"]);
                  this.barChartLabels2.push("Temporada "+ pro[index]["temporada_id"]);
                }
              }
            });
        },
        error => {
          console.log(error)
        });
  }

  barChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          max: 1000,
          min: 0
        }
      }]
    }
  };
  barChartLabels: Label[] = [];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  lineChartColors: Color[] = [
    {
      backgroundColor: 'lightgreen',
    },
  ];
  barChartData: ChartDataSets[] = [
    { data: this.datos, label: 'Kilos' }
  ];

  barChartOptions2: ChartOptions = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          max: 1000,
          min: 0
        }
      }]
    }
  };
  barChartLabels2: Label[] = [];
  barChartType2: ChartType = 'bar';
  barChartLegend2 = true;
  lineChartColors2: Color[] = [
    {
      backgroundColor: 'lightgreen',
    },
  ];
  barChartData2: ChartDataSets[] = [
    { data: this.datos2, label: 'Kilos' }
  ];
}

