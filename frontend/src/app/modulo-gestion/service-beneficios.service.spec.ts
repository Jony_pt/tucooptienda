import { TestBed } from '@angular/core/testing';

import { ServiceBeneficiosService } from './service-beneficios.service';

describe('ServiceBeneficiosService', () => {
  let service: ServiceBeneficiosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceBeneficiosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
