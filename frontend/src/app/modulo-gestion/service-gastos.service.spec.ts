import { TestBed } from '@angular/core/testing';

import { ServiceGastosService } from './service-gastos.service';

describe('ServiceServiciosService', () => {
  let service: ServiceGastosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceGastosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
