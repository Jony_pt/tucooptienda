import { Injectable } from '@angular/core';
import { UrlAPI } from "../services/api.service";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { AccountService } from '../account/account-service.service';
@Injectable({
  providedIn: 'root'
})
export class ServiceProductosService {
  usuario: any = {};
  constructor(private http: HttpClient, private AccountService: AccountService) { }

  public getNoticias(): Observable<any> {
    this.AccountService.user.subscribe(
      data => {
        this.usuario = data;
      }
    );
    return this.http.get(UrlAPI.baseUrl + "productos-usuario?usuario=" + this.usuario.id);
  }
}
