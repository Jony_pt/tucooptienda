import { TestBed } from '@angular/core/testing';

import { ServiceServiciosService } from './service-servicios.service';

describe('ServiceServiciosService', () => {
  let service: ServiceServiciosService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServiceServiciosService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
