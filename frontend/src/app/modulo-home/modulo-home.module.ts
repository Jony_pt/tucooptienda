import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoticiasComponent } from './noticias/noticias.component';
import { ServiciosComponent } from './servicios/servicios.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [NoticiasComponent, ServiciosComponent, HomeComponent],
  imports: [
    CommonModule,
  ],
  exports : [
    NoticiasComponent,
    ServiciosComponent,
    HomeComponent
  ]
})
export class ModuloHomeModule { }