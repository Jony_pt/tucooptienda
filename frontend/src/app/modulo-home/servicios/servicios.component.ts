import { Component, OnInit } from '@angular/core';
import { ServiciosService } from '../../modulo-servicios/servicios.service';

@Component({
  selector: 'app-servicios',
  templateUrl: './servicios.component.html',
  styleUrls: ['./servicios.component.scss']
})
export class ServiciosComponent implements OnInit {
  servicios:any []=[];
  constructor(private serviciosService:ServiciosService) { }

 /* servicios: any =[
    {'id' : 1,
    'titulo' : "servicio 1",
    'texto': 'explicacion servicio 1'
    },
    {'id' : 2,
    'titulo' : "servicio 2",
    'texto' : 'explicacion servicio 2'
    },
    {'id' : 3,
    'titulo' : "servicio 3",
    'texto' : 'explicacion servicio 3'
    }
  ]*/
  
  //ngOnInit(): void {  }
  ngOnInit(): void {
    this.serviciosService.getServicios()
    .subscribe(
      data => {
        this.servicios = data;
        console.log('servicios',this.servicios);
      },
      error=>{
        console.log(error)
      }
      );  
  
    console.log(this.servicios);
  }
}
