import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListaNoticiasComponent } from './lista-noticias/lista-noticias.component';



@NgModule({
  declarations: [ListaNoticiasComponent],
  imports: [
    CommonModule
  ],
  exports: [
    ListaNoticiasComponent
  ]
})
export class ModuloNoticiasModule { }
