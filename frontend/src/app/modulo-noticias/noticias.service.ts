import { Injectable } from '@angular/core';
import { UrlAPI } from "../services/api.service";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
@Injectable({
  providedIn: 'root'
})
export class NoticiasService {
  //private noticias: Noticias = { id: "", fecha_hora: "", titular: "", cuerpo: "", imagen_src: "" };
  // public noticias: Noticias = {array:[]};

  constructor(private http: HttpClient) { }

  public getNoticias(): Observable<any>{
  
    return this.http.get(UrlAPI.baseUrl+"noticias");
    
  }
}
