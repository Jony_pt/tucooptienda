import { Component, OnInit } from '@angular/core';
import { ServiciosService } from '../../modulo-servicios/servicios.service';
@Component({
  selector: 'app-lista-servicios',
  templateUrl: './lista-servicios.component.html',
  styleUrls: ['./lista-servicios.component.scss']
})
export class ListaServiciosComponent implements OnInit {
  servicios:any []=[];
  constructor(private serviciosService:ServiciosService) { }
  ngOnInit(): void {
    this.serviciosService.getServicios()
    .subscribe(
      data => {
        this.servicios = data;
        console.log('servicios',this.servicios);
      },
      error=>{
        console.log(error)
      }
      );  
  
    console.log(this.servicios);
    console.log(localStorage.getItem("user"));
    
  }

}
