import { Injectable } from '@angular/core';
import { UrlAPI } from "../services/api.service";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError, retry } from 'rxjs/operators';
import { AccountService } from '../account/account-service.service';

@Injectable({
  providedIn: 'root'
})
export class ArticulosService {
  usuario: any = {};
  constructor(private http: HttpClient, private accountService: AccountService) { }

  public getArticulos(): Observable<any> {
    return this.http.get(UrlAPI.baseUrl + "articulos");
  }

  public getFacturausuario(): Observable<any> {
    this.accountService.user.subscribe(
      data => {
        this.usuario = data;
      }
    );
    return this.http.get(UrlAPI.baseUrl + "factura?usuario=" + this.usuario.id);
  }

  public getlineaFactura(): Observable<any>{
    return this.http.get(UrlAPI.baseUrl + "linea-factura");
  }

  public getFactura(): Observable<any> {
    return this.http.get(UrlAPI.baseUrl + "factura");
  }

  public cantidadProductousuario(id: any, cantidad: any) {
    return this.http.post(UrlAPI.baseUrl + "productos-usuario/cantidad", { id, cantidad });
  }

  createFactura(id_usuario: any, fecha_compra: any, estado: any, fecha_envio: any, direccion_envio: any, total: any): Observable<any> {
    return this.http.post<any>(UrlAPI.baseUrl + "factura/facturas", { id_usuario, fecha_compra, estado, fecha_envio, direccion_envio, total })
      .pipe(
        catchError(err => err = "error")
      );
  }

  

  postCreateLineafactura(id_articulo: any, cant_prod: any, id_factura: any) {
    return this.http.post<any>(UrlAPI.baseUrl + "linea-factura/factura", { id_articulo, cant_prod, id_factura })
      .pipe(
        catchError(err => err)
      );

  }
}
