import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { items } from '../../../models/items';


@Component({
  selector: 'app-card-list-product',
  templateUrl: './card-list-product.component.html',
  styleUrls: ['./card-list-product.component.scss']
})
export class CardListProductComponent implements OnInit {
  @Input() products!: items;
  
  constructor(private router: Router) { }
 

  ngOnInit(): void {
    console.log("hola",this.products);
    
  }

  public onProductClicked(){
    console.log("id", this.products.id);
    this.router.navigate(['/viewProducts',  this.products.id] );
    

  }

}
