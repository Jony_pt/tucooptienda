import { Component, OnInit } from '@angular/core';
import { ArticulosService } from '../articulos.service';
import { 
  DxScrollViewComponent,
 } from 'devextreme-angular/ui/scroll-view';

@Component({
  selector: 'app-card-list',
  templateUrl: './card-list.component.html',
  styleUrls: ['./card-list.component.scss']
})
export class CardListComponent implements OnInit {

  constructor(private articulosService: ArticulosService) { }

  articulos: any[] = [];
  
  ngOnInit(): void {
    let articulo: any[] = [];
    this.articulosService.getArticulos()
    .subscribe(
      data => {
        this.articulos = data;

        articulo = Object(this.articulos)

        this.articulos = articulo;

        console.log("binding",this.articulos);

      }
    )

    
  }

}
