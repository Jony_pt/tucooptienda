import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TiendaComponent } from './tienda/tienda.component';
import { ViewProductsComponent } from './view-products/view-products.component';
import { FormsModule } from '@angular/forms';
import { CardListComponent } from './card-list/card-list.component';
import { CardListProductComponent } from './card-list/card-list-product/card-list-product.component';
import { DxScrollViewModule } from 'devextreme-angular';
import { DxCheckBoxModule } from 'devextreme-angular';
import { DxRangeSliderModule } from 'devextreme-angular';
import { DxSliderModule } from 'devextreme-angular';
import { DxNumberBoxModule } from 'devextreme-angular';
import { DxAutocompleteModule } from 'devextreme-angular';




@NgModule({
  declarations: [TiendaComponent, ViewProductsComponent, CardListComponent, CardListProductComponent],
  imports: [
    CommonModule,
    FormsModule,
    DxScrollViewModule,
    DxCheckBoxModule,
    DxRangeSliderModule,
    DxSliderModule,
    DxNumberBoxModule,
    DxAutocompleteModule
  ]
})
export class ModuloTiendaModule { }
