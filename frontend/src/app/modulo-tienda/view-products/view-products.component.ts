import { Component, OnInit } from '@angular/core';
import { ArticulosService } from '../articulos.service';
import { ActivatedRoute } from '@angular/router';
import { CarritoService } from 'src/app/modulo-carrito/carrito.service';
import { items } from '../../models/items';
import { element } from 'protractor';
import { AnyTxtRecord } from 'dns';

@Component({
  selector: 'app-view-products',
  templateUrl: './view-products.component.html',
  styleUrls: ['./view-products.component.scss']
})
export class ViewProductsComponent implements OnInit {

  constructor(private articulosService: ArticulosService, private activaterouter: ActivatedRoute, private carritoService: CarritoService ) { }
  producto!: any;
  articulos: any[] = [];
  totalProducts = 0;
  incorrecto: any;
  cantidad: any;
  ngOnInit(): void {
    let articulo: any[] = [];
    this.articulosService.getArticulos()
    .subscribe(
      data => {
        this.articulos = data;

        articulo = Object(this.articulos);

        if(this.activaterouter.snapshot.params){
          this.producto = articulo.filter(
            (element) => element.id == Number(this.activaterouter.snapshot.params.id)
          )[0];
          this.cantidad = this.producto.cantidad;
          console.log("producto", this.producto);
          console.log("ruta",this.activaterouter.snapshot.params);
          console.log("datos", articulo.filter(
            (element) => element.id == this.activaterouter.snapshot.params.id
          )[0]);
    
        }else{
          console.log("adios");
        }
    
      }
    )

 
    console.log("prueba",this.articulos,"o",articulo);
  }
  addItems(product: any) {
    console.log("total", product);
    console.log("precio", product.precio);
    if (this.totalProducts <= 0) {
      this.incorrecto = "Cantidad incorrecta";

    } else {
      this.incorrecto = "";
      this.carritoService.changeCart(product, this.totalProducts);
      this.totalProducts = 0;
    }




  }

    
  }


