import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PerfilDPComponent } from './perfil-dp.component';

describe('PerfilDPComponent', () => {
  let component: PerfilDPComponent;
  let fixture: ComponentFixture<PerfilDPComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PerfilDPComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PerfilDPComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
