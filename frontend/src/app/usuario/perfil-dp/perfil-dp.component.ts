import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import {AccountService} from '../../account/account-service.service';

@Component({
  selector: 'app-perfil-dp',
  templateUrl: './perfil-dp.component.html',
  styleUrls: ['./perfil-dp.component.scss']
})
export class PerfilDPComponent {
  usuario: any = {};
  
  constructor(private AccountService: AccountService) { 
  }

  ngOnInit(): void{
    
    this.AccountService.user.subscribe(
      data => {
        this.usuario = data;
      }
    );
  }
}
