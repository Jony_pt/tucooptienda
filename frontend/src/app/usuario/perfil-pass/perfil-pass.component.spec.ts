import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PerfilPassComponent } from './perfil-pass.component';

describe('PerfilPassComponent', () => {
  let component: PerfilPassComponent;
  let fixture: ComponentFixture<PerfilPassComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PerfilPassComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PerfilPassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
