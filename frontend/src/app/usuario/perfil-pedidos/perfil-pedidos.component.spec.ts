import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PerfilPedidosComponent } from './perfil-pedidos.component';

describe('PerfilPedidosComponent', () => {
  let component: PerfilPedidosComponent;
  let fixture: ComponentFixture<PerfilPedidosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PerfilPedidosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PerfilPedidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
