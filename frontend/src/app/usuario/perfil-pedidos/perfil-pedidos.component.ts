import { Component, OnInit } from '@angular/core';
import { ArticulosService } from '../../modulo-tienda/articulos.service';
import { BehaviorSubject, Observable } from 'rxjs';
//import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
//import {ModalFacturaComponent } from '../modal-factura/modal-factura.component';


@Component({
  selector: 'app-perfil-pedidos',
  templateUrl: './perfil-pedidos.component.html',
  styleUrls: ['./perfil-pedidos.component.scss']
})
export class PerfilPedidosComponent implements OnInit {
  public facturas: any[] = [];
  public factura: any;
  constructor(private articulosService: ArticulosService, /*private dialog: MatDialog*/) { }

  ngOnInit(): void {

    this.articulosService.getFacturausuario().subscribe(
      data =>{
        this.facturas = data;

        this.factura = Object(this.facturas);

      }

    )
  }
 /*  openDialog(factura: any): void {
    const dialogRef = this.dialog.open(ModalFacturaComponent, {
      width: '75%',
      data: {factura}
    });
    dialogRef.afterClosed().subscribe(data => {});*/
    
/*
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  }*/

}
