import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PerfilPrivacyComponent } from './perfil-privacy.component';

describe('PerfilPrivacyComponent', () => {
  let component: PerfilPrivacyComponent;
  let fixture: ComponentFixture<PerfilPrivacyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PerfilPrivacyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PerfilPrivacyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
