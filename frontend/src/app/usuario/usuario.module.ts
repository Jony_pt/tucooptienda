import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfilPassComponent } from './perfil-pass/perfil-pass.component';
import { PerfilDPComponent } from './perfil-dp/perfil-dp.component';
import { PerfilPrivacyComponent } from './perfil-privacy/perfil-privacy.component';
import { PerfilPedidosComponent } from './perfil-pedidos/perfil-pedidos.component';



@NgModule({
  declarations: [ PerfilPassComponent, PerfilDPComponent, PerfilPrivacyComponent, PerfilPedidosComponent],
  imports: [
    CommonModule
  ],
  exports: [
  PerfilDPComponent,
  PerfilPassComponent,
  PerfilPrivacyComponent]
})
export class UsuarioModule { }
